Rails.application.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'
  namespace :admin do
    resources :users, :products, :blogs
  end

  resources :products, except: [:edit]
  
  resources :blogs do
    resources :comments
  end

  resources :pages do
    collection do
      post :url_preview
    end
  end

  resources :pages do
    resources :pagecomments
  end  

  namespace :admin do
    resources :products do 
      resources :images
    end
  end

  get 'users/index'

  get 'pages/show'

  devise_for :users, controllers: { sessions: 'users/sessions' }
  root to: "pages#index"

  match '*path', to: redirect('/404'), via: :all
 
  if Rails.env.production?
    get '404', :to => 'application#page_not_found'
 end
end
