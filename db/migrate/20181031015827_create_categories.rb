class CreateCategories < ActiveRecord::Migration[5.1]
  def change
    create_table :categories do |t|
      t.string :name, length: { minimum: 3 }

      t.timestamps
    end
  end
end
