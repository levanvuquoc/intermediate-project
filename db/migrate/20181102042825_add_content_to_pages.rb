class AddContentToPages < ActiveRecord::Migration[5.1]
  def change
    add_column :pages, :content, :string
    add_column :pages, :image_url, :string
    add_column :pages, :video_url, :string
  end
end
