class AddTitleAuthorToPage < ActiveRecord::Migration[5.1]
  def change
    add_column :pages, :title, :string
    add_column :pages, :author, :string
    add_column :pages, :body, :text
  end
end
