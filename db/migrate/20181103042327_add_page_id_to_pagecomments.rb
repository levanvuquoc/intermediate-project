class AddPageIdToPagecomments < ActiveRecord::Migration[5.1]
  def change
    add_column :pagecomments, :page_id, :integer
  end
end
