# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Blog.create(post: 'Nala', content: 'Born to be wild')
Blog.create(post: 'Alex', content: 'Calm as can be')
Blog.create(post: 'Leroy', content: 'Life of the pawty')
Blog.create(post: 'Belle', content: 'Miss Independent')