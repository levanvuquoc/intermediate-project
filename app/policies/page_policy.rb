class PagePolicy < ApplicationPolicy
  def index
    true
  end
  
  def create?
    true
  end

  def new?
    true
  end

  def destroy?
    user.admin? if user
  end
end