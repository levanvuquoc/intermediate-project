class BlogPolicy < ApplicationPolicy
  def create?
    user
  end

  def destroy?
    user.admin? if user
  end
end