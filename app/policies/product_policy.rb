class ProductPolicy < ApplicationPolicy

  def index?
    user
  end

  def show?
    user
  end

  def new?
    user.admin? if user
  end

  def update?
    user.admin? if user
  end

  def edit?
    user.admin? if user
  end

  def destroy?
    user.admin? if user
  end
end