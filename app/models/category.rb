class Category < ApplicationRecord
  has_many :references
  has_many :product, through: :references
  validates_length_of :name, :minimum=>3, :too_short => "Please enter at least 3 character"
end
