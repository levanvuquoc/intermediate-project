class Blog < ApplicationRecord
  validates :post, presence: true
  validates :content, presence: true

  mount_uploaders :images, ImageUploader

  has_many :comments, dependent: :destroy
end
