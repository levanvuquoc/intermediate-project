class Pagecomment < ApplicationRecord
  belongs_to :page

  validates_length_of :name, :minimum=>3, :too_short => "Please enter at least 3 character"
  validates_length_of :body, :minimum=>10, :too_short => "Please enter at least 10 character"
end
