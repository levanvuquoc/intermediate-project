// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require bootstrap-sprockets
//= require jquery_ujs
//= require ckeditor/init
//= require_tree .
$(document).ready(function(){
  $("#image_remote_url").on('paste', function(e) {
    setTimeout(function(){
      console.log($(e.target).val())
      text = $("#image_remote_url").val();
      console.log(  $("#image_remote_url").val() )
      console.log(text)
      // send url to service for parsing
        $.ajax({
          url: "/pages/url_preview",
          dataType: "json",
          type: 'POST',
          data: { url: text },
          success: function(url_preview) {
            console.log(url_preview)
            $("#url_preview").html(url_preview.url);
            $("#title_preview").html(url_preview.title);
            $("#image_preview").html('<img src="' + url_preview.image_url + '" />');
            $("#description_preview").html(url_preview.description);
          },
          error: function() { console.log("error"); }
      });
    }, 100)
  });
})


$(document).ready(function(){
  $('#sub-db').click(function(){
    url = $('#sub-db').data("url")
      $.ajax({
        url: url,
        method: "get",
        dataType: "json",
        data:{
          page:{
            title: $("#title_preview").text(),
            image_url: $("#image_preview>img").attr("src"),
            content: $("#description_preview").text(),
          }
        },
        success: function(data){
          console.log(data)
          window.location.href = "/pages"
      }
    })
  })
})