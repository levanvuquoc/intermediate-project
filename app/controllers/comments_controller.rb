class CommentsController < ApplicationController
  def create
    authorize @blog
    @blog = Blog.find(params[:blog_id])
    
    @comment = @blog.comments.new(comment_params)
    respond_to do |format|
      if @comment.save
        format.html { redirect_to @blog }
        format.js
      else
        format.html { redirect_to @blog }
        format.js
      end
    end
  end

  def destroy
    authorize @blog
    @blog = Blog.find(params[:blog_id])
    
    @comment = @blog.comments.find(params[:id])
    @comment.destroy

    redirect_to blog_path(@blog)
  end

  private 
  def comment_params
    params.require(:comment).permit(:name, :body)
  end
end
