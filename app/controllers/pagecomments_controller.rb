class PagecommentsController < ApplicationController
  def create
    @page = Page.find(params[:page_id])
    @pagecomment = @page.pagecomments.new(pagecomment_params)

    respond_to do |format|
      if @pagecomment.save
        format.html { redirect_to page_path(@page) }
        format.js
      else
        format.html { redirect_to page_path(@page) }
        format.js
      end
    end
  end
  
  def show
    @page = Page.find(params[:page_id])
    @pagecomment = @page.pagecomments.new
  end

  def destroy
    authorize @page
    @page = Page.find(params[:page_id])
    
    @pagecomment = @page.pagecomments.find(params[:id])
    @pagecomment.destroy

    redirect_to page_path(@page)
  end

  private
  def pagecomment_params
    params[:pagecomment].permit(:name, :body)
  end
  
end
