class ApplicationController < ActionController::Base
  include Pundit
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :set_locale
  
  rescue_from ActionController::RoutingError, :with => :redirect_404
  # protect_from_forgery
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized
  protected
 
  def configure_permitted_parameters
    added_attrs = [:username, :email, :password, :password_confirmation, :remember_me, :image]
    devise_parameter_sanitizer.permit :sign_up, keys: added_attrs
    devise_parameter_sanitizer.permit :account_update, keys: added_attrs
  end

  private

  def render_404
    render :file => "#{Rails.root}/public/404.html",  :status => 404
  end

  def set_locale
    I18n.locale = params[:locale] if params[:locale].present?
  end

  def user_not_authorized(exception)
    policy_name = exception.policy.class.to_s.underscore
 
    flash[:alert] = t "#{PagePolicy}.#{exception.query}", scope: "pundit", default: :default
    redirect_to root_path
  end  

  def redirect_404
     p "123"
    redirect_to "/404"
  end
end