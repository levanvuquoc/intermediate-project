class PagesController < ApplicationController
  skip_before_action :verify_authenticity_token, :only => [:create]
  before_action :set_page, only: [:show, :edit, :update, :destroy]
  def show
    @page = Page.find params[:id]
    @pagecomment = @page.pagecomments.new
    respond_to do |format|
        format.html
        format.json { render json: @page.to_json} 
    end
  end

  def url_preview
    url = params[:url]
    @rs = Page.parse_url(url)
    respond_to do |format|
      format.html
      format.json { render json: @rs} 
    end 
  end

  def index
    @pages = Page.paginate(:page => params[:page], :per_page => 3)
  end

  def create
  end

  def new
    puts params
    
    @page = Page.new(page_params)
    @page.save
    
    respond_to do |format|
      format.html { }  
      format.json { render json: {data: "success"} }
      
    end
  end

  def destroy
    @page.destroy

    respond_to do |format|
      format.html { redirect_to pages_path, notice: 'Product was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  private

    def page_params
      params.require(:page).permit(:title, :content, :image_url)
    end

    def set_page
      @page = Page.find_by id: params[:id]
    end
end