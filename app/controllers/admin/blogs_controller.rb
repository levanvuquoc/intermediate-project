class Admin::BlogsController < Admin::BaseController
  layout "admin_applications"
  before_action :set_blog, only: [:show, :edit, :update, :destroy]
  def index
    @blogs = Blog.paginate(:page => params[:page], :per_page => 3)
  end

  def new
    @blog = Blog.new
  end

  def create
    @blog = Blog.new(blog_params)
    respond_to do |format|
      if @blog.save
        format.html { redirect_to admin_blogs_path, notice: 'Blog was successfully created.' }
        format.json { render :show, status: :created, location: @blog }
      else
        format.html { render :new }
        format.json { render json: @blog.errors, status: :unprocessable_entity }
      end
    end    
  end

  def update
    respond_to do |format|
      if @blog.update(blog_params)
        format.html { redirect_to admin_blogs_path, notice: 'Blog was successfully created.' }
        format.json { render :show, status: :created, location: @blog }
      else
        format.html { render :new }
        format.json { render json: @blog.errors, status: :unprocessable_entity }
      end
    end  
  end

  def edit
  end

  def destroy
    @blog.destroy
    respond_to do |format|
      format.html { redirect_to admin_blogs_path, notice: 'Product was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def show
  end

  private

  def blog_params
    params.require(:blog).permit(:post, :content, {images: []})
  end
  
  def set_blog
  @blog = Blog.find_by id: params[:id]
  end

end