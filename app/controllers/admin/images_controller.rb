class Admin::ImagesController < Admin::BaseController
  before_action :set_product, only: [:show, :update, :destroy]
  # before_action :set_image
  def update
    uploader = ImageUploader.new(@product, "images")
    @product = Product.find(params[:product_id])
    uploader.store!(params[:image])
    uploader.retrieve_from_store!(params[:image].original_filename)
    image_name = params[:image].original_filename
    @i = params[:id].to_i
    remain_images = @product.images # copy initial avatars
    remain_images[@i] = uploader
    @product.images = remain_images # re-assign back
    @product.save
    respond_to do |format|

        format.html { redirect_to admin_product_path(@product), alert: 'Update Image Successfully' }
        format.json { head :no_content }
    end
  end

  def destroy
    respond_to do |format|
      @i = params[:id].to_i
      if @product.images.count > 1
        remain_images = @product.images # copy initial avatars
        delete_image = remain_images.delete_at(@i) # delete the target image
        delete_image.try(:remove!) # delete image
        @product.images = remain_images # re-assign back
        @product.save  

        format.html { redirect_to admin_product_path(@product), alert: 'Delete successfully.' }
        format.json { head :no_content }
      else
        format.html { redirect_to admin_product_path(@product), alert: 'Product has at least 1 image.' }
        format.json { head :no_content }
      end
    end
  end

  private

  def set_product
    @product = Product.find(params[:product_id])
  end

  def set_image
    @image = @product.images[params[:id]]
  end

  def image_params
    params.require(:product).permit({images: []})   
  end
end