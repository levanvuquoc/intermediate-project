class Admin::ProductsController < Admin::BaseController
  layout "admin_applications"

  before_action :set_product, only: [:show, :edit, :update, :destroy]

  def index
    authorize @products
    @products = Product.paginate(:page => params[:page], :per_page => 3)
  end

  def show
  end

  # GET /products/new
  def new
    authorize @product
    @product = Product.new
  end

  # GET /products/1/edit
  def edit
  end

  # POST /products
  # POST /products.json
  def create
    authorize @product
    @product = Product.new(product_params)
    respond_to do |format|
      if @product.save
        format.html { redirect_to admin_products_path, notice: 'Product was successfully created.' }
        format.json { render :show, status: :created, location: @product }
      else
        format.html { render :new }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /products/1
  # PATCH/PUT /products/1.json
  def update
    authorize @product
    respond_to do |format|
      if @product.update(product_params)
        format.html { redirect_to admin_products_path, notice: 'Product was successfully updated.' }
        format.json { render :show, status: :ok, location: @product }
      else
        format.html { render :edit }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # def delete_picture
  #   respond_to do |format|
  #     @i = params[:i].to_i
  #     if @i != 0
  #       remain_images = @product.images # copy initial avatars
  #       delete_image = remain_images.delete_at(@i) # delete the target image
  #       delete_image.try(:remove!) # delete image
  #       @product.images = remain_images # re-assign back
  #       @product.save  

  #       format.html { redirect_to admin_product_path, alert: 'Test' }
  #       format.json { head :no_content }@product = Product.new(product_params
  #     else
  #       format.html { redirect_to admin_@product = Product.new(product_paramsast 1 image' }
  #       format.json { head :no_content }@product = Product.new(product_params
  #     end
  #   end
  # end

  # def delete_picture
  #   respond_to do |format|
  #     @i = params[:i].to_i
  #     if @i != 0
  #       remain_images = @product.images # copy initial avatars
  #       delete_image = remain_images.delete_at(@i) # delete the target image
  #       delete_image.try(:remove!) # delete image
  #       @product.images = remain_images # re-assign back
  #       @product.save  

  #       format.html { redirect_to admin_product_path, alert: 'Test' }
  #       format.json { head :no_content }
  #     else
  #       format.html { redirect_to admin_product_path, alert: 'Product has at least 1 image' }
  #       format.json { head :no_content }
  #     end
  #   end
  # end

  # DELETE /products/1
  # DELETE /products/1.json
  def destroy
    authorize @product
    @product.destroy
    respond_to do |format|
      format.html { redirect_to admin_products_path, notice: 'Product was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product
      @product = Product.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def product_params
      params.require(:product).permit(:name, :description, :slug, {images: []})
    end
end