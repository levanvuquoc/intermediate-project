class Admin::UsersController < Admin::BaseController
  layout "admin_applications"
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  def index
    @users = User.all
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
        redirect_to admin_users_path, alert: "User created successfully."
    else
      respond_to do |format|
        format.html { render :new }
      end
    end
  end

  def edit
    if current_user == @user || !@user.admin?
    else
      flash[:errors] = "Can not edit other admin profile"
      redirect_to admin_users_path
    end

  end

  def update
    if @user.update_attributes(user_params)
      redirect_to admin_users_path, alert: "User update successfully."
    else
      respond_to do |format|
        format.html { render :edit }
      end
    end
  end

  def show
  end
  
  def destroy 
    @user.destroy

    redirect_to admin_users_path, notice: "Delete success"
  end

  private

  def user_params
    params.require(:user).permit(:username, :email, :password, :role, :password_confirmation, :image)    
  end

  def set_user
    @user = User.find_by id: params[:id]
  end
  
end


