class Admin::BaseController < ApplicationController
  before_action :authenticate_user!
  before_action :check_authenticated
  

  def check_authenticated
    if current_user.role != "admin"
      redirect_to root_path
    end
  end
end