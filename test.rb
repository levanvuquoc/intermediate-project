require 'open-uri'
require 'nokogiri'
def param_rs
    def param_url
        url = 'http://www.nokogiri.org/tutorials/parsing_an_html_xml_document.html'
    end

    doc = Nokogiri::HTML(open(param_url), nil, 'UTF-8')
                rs = {}
    title = ""
    description = ""
    url = ""
    image_url = ""
    
    doc.xpath("//head//meta").each do |meta|
        if meta['property'] == 'og:title'
            title = meta['content']
        elsif meta['property'] == 'og:description' || meta['name'] == 'description'
            description = meta['content']
        elsif meta['property'] == 'og:url'
            url = meta['content']
        elsif meta['property'] == 'og:image'
            image_url = meta['content']
        end
    end
    
    if title == ""
        title_node = doc.at_xpath("//head//title")
        if title_node
            title = title_node.text
        elsif doc.title
            title = doc.title
        else
            title = param_url
        end
    end
    
    if description ==""
        #maybe search for content from BODY
        description = title
    end
    
    if url ==""
        url = param_url
    end
    

    rs[:title] = title 
    rs[:description] = description
    rs[:url] = url
    rs[:image_url] = image_url

    rs
end